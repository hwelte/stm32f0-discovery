#include <string.h>

#include "stm32f0xx_conf.h"
#include "stm32f0xx_rtc.h"
#include "stm32f0xx_usart.h"

static int puts(const char *str)
{
	const char *p = str;
	char c;

	for (c = *p++; c; c = *p++) {
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) != SET) ;
		USART_SendData(USART1, c);
	}
}

static char bcd2char(uint8_t bcd)
{
	if (bcd < 0xA)
		return '0' + bcd;
	else
		return 'A' + (bcd - 0xA);
}

static void bcd2str(char *out, uint8_t bcd)
{
	*out++ = bcd2char(bcd >> 4);
	*out++ = bcd2char(bcd & 0xF);
}


void SysTick_Handler(void) {
  static uint16_t tick = 0;

  switch (tick++) {
  	case 100:
  		tick = 0;
  		GPIOC->ODR ^= (1 << 8);
  		break;
	case 25:
		USART_SendData(USART1, 'A');
		break;
	case 75:
		USART_SendData(USART1, 'z');
		break;
	case 50:
		{
			RTC_TimeTypeDef time;
			char buf[128];

			RTC_GetTime(RTC_Format_BCD, &time);
			memset(buf, 0, sizeof(buf));
			//sprintf(buf, "%02x:%02x:%02x\r\n", time.RTC_Hours, time.RTC_Minutes, time.RTC_Seconds);
			bcd2str(buf, time.RTC_Seconds);
			strcpy(buf+2, "s\r\n");

			puts(buf);
		}
		break;
  }
}

static GPIO_InitTypeDef gpioa9 = {
	.GPIO_Pin	= GPIO_Pin_9 | GPIO_Pin_10,
	.GPIO_Mode	= GPIO_Mode_AF,
	.GPIO_Speed	= GPIO_Speed_Level_1,
	.GPIO_OType	= GPIO_OType_PP,
	.GPIO_PuPd	= GPIO_PuPd_UP,
};

static RTC_InitTypeDef rtcinit = {
	.RTC_HourFormat = RTC_HourFormat_24,
	.RTC_AsynchPrediv = (128-1),
	.RTC_SynchPrediv = (256-1),
};

static USART_InitTypeDef usartinit = {
	.USART_BaudRate = 115200,
	.USART_WordLength = USART_WordLength_8b,
	.USART_StopBits = USART_StopBits_1,
	.USART_Parity = USART_Parity_No,
	.USART_Mode = USART_Mode_Rx | USART_Mode_Tx,
	.USART_HardwareFlowControl = USART_HardwareFlowControl_None,
};

int main(void)
{

	RCC->AHBENR |= RCC_AHBENR_GPIOCEN; 	// enable the clock to GPIOC
						//(RM0091 lists this as IOPCEN, not GPIOCEN)

	GPIOC->MODER = (1 << 16);

	/* UART */
	//RCC_USARTCLKConfig(RCC_USART1CLK_SYSCLK);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);
	GPIO_Init(GPIOA, &gpioa9);
	USART_Init(USART1, &usartinit);
	USART_Cmd(USART1, ENABLE);

#if 1
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	PWR_BackupAccessCmd(ENABLE);

	/* Low-Speed External Oscillator */
	RCC_LSEConfig(RCC_LSE_ON);
	RCC_LSEDriveConfig(RCC_LSEDrive_High);
	/* wait for LSERDY */
	while (!(RCC_GetFlagStatus(RCC_FLAG_LSERDY)));
#else
	RCC_LSICmd(ENABLE);
	while (!(RCC_GetFlagStatus(RCC_FLAG_LSIRDY)));
#endif

	/* RTC */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
	RCC_RTCCLKCmd(ENABLE);
	RTC_Init(&rtcinit);

	SysTick_Config(SystemCoreClock/100);

	puts("\r\nEntering main...\r\n");

	while(1);

}
